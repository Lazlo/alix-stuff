# Stuff related to the PC Engines ALIX Board

[PC Engines ALIX2.D13](https://www.pcengines.ch/alix2d13.htm)

## Configure ALIX for Network Boot

### Host Setup

On Debian, run the following command to install DNSMASQ, bootloader and the NFS kernel server.

```
sudo apt-get -q install -y dnsmasq pxelinux nfs-kernel-server
```

Disable DNSMASQ from starting automaticaly.

```
sudo systemctl stop dnsmasq
sudo systemctl mask dnsmasq
```

Configure the network interface that is connected to the ALIX device.

```
allow-hotplug eth21
iface eth21 inet static
	address 21.0.0.1
	netmask 255.255.255.0
```

Apply network interface settings.

```
sudo ifup eth21
```

FIXME create NFS directories
FIXME add NFS shares
FIXME tweak NFS server to support NFS v2
FIXME prepare tftp root directory

```
d=/srv/tftp/alix-boot
sudo mkdir -p $d $d/pxelinux.cfg
cp default $d/pxelinux.cfg

PXELINUX0_FILE="$(dpkg-query -L pxelinux | grep '/pxelinux.0')"
LDLINUX_FILE="$(dpkg-query -L syslinux-common | grep ldlinux.c32)"
MENU_FILE="$(dpkg-query -L syslinux-common | grep 'bios/menu.c32')"
LIBUTIL_FILE="$(dpkg-query -L syslinux-common | grep 'bios/libutil.c32')"
bootfiles="$PXELINUX0_FILE $LDLINUX_FILE $MENU_FILE $LIBUTIL_FILE"

sudo ln -s $bootfiles $d
```

Start DNSMASQ

```
dnsmasq --no-daemon --conf-file=dnsmasq.conf
```

### Device Configuration and Boot

Connect a network cable from the network interface of the server to the port next to the USB connectors of the ALIX board. Only on this network interface booting from the network is possible.

Configure the BIOS for network booting.

 * press "s" during initial memory test, to enter ALIX BIOS
 * press "e" to enable PXE boot
 * press "q" to quit
 * press "y" to save changes
